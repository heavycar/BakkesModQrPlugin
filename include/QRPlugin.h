#pragma once
#pragma comment(lib, "bakkesmod.lib")

#ifdef _MSC_VER
#pragma warning(disable : 251 244)
#endif
#include "bakkesmod/plugin/bakkesmodplugin.h"
#ifdef _MSC_VER
#pragma warning(default : 251 244)
#endif

namespace QRPlugin {

class QRPlugin : public BakkesMod::Plugin::BakkesModPlugin {
  private:
    bool visible = true;
    uint32_t frame_counter = 0;
    LARGE_INTEGER perf_freq = {0};
    int x_pos = 0;
    // int y_pos = 962;
    // int scale = 4;
    int y_pos = 891;
    int scale = 7;
    int alpha = 255;
    int ecc = 1;

  public:
    virtual void onLoad();
    virtual void onUnload();
    void on_draw(CanvasWrapper canvas);
};

} // namespace QRPlugin