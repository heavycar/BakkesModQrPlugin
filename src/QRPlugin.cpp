#include "fmt/format.h"

#include "QRPlugin.h"
#include "QrCode.hpp"
// #ifdef _MSC_VER
// #pragma warning(disable : 251 244)
// #endif
// #include "bakkesmod/wrappers/GameEvent/TutorialWrapper.h"
// #include "bakkesmod/wrappers/GameObject/CarWrapper.h"
// #include "bakkesmod/wrappers/ReplayServerWrapper.h"
// #ifdef _MSC_VER
// #pragma warning(default : 251 244)
// #endif
#include <string>

using qrcodegen::QrCode;

namespace QRPlugin {

BAKKESMOD_PLUGIN(QRPlugin, "QR Plugin", "0.2", PLUGINTYPE_SPECTATOR)

void QRPlugin::onLoad() {
    constexpr auto PERMISSION = PERMISSION_MENU;
    QueryPerformanceFrequency(&perf_freq);
    gameWrapper->RegisterDrawable([&](auto canvas) { on_draw(canvas); });
    cvarManager->registerNotifier("qr_toggle", [&](auto) { visible = !visible; },
                                  "Toggle the QR code display.", PERMISSION);
    cvarManager->registerCvar("qr_x", std::to_string(x_pos));
    cvarManager->registerNotifier("qr_x", [&](auto) { x_pos = cvarManager->getCvar("qr_x").getIntValue(); },
                                  "Set the X position of the QR code.", PERMISSION);
    cvarManager->registerCvar("qr_y", std::to_string(y_pos));
    cvarManager->registerNotifier("qr_y", [&](auto) { y_pos = cvarManager->getCvar("qr_y").getIntValue(); },
                                  "Set the Y position of the QR code.", PERMISSION);
    cvarManager->registerCvar("qr_scale", std::to_string(scale));
    cvarManager->registerNotifier(
        "qr_scale", [&](auto) { scale = std::max(0, cvarManager->getCvar("qr_scale").getIntValue()); },
        "Set the scale of the QR code.", PERMISSION);
    cvarManager->registerCvar("qr_alpha", std::to_string(alpha));
    cvarManager->registerNotifier(
        "qr_alpha",
        [&](auto) { alpha = std::min(255, std::max(0, cvarManager->getCvar("qr_alpha").getIntValue())); },
        "Set the opacity of the QR code.", PERMISSION);
    cvarManager->registerCvar("qr_ecc", std::to_string(ecc));
    cvarManager->registerNotifier("qr_ecc",
                                  [&](auto) {
                                      ecc = std::min(
                                          3, std::max(0, cvarManager->getCvar("qr_ecc").getIntValue()));
                                      cvarManager->getCvar("qr_ecc").setValue(ecc);
                                  },
                                  "Set the error correction level of the QR code.", PERMISSION);
}

void QRPlugin::on_draw(CanvasWrapper canvas) {
    frame_counter++;
    if (visible) {
        LARGE_INTEGER timestamp_raw;
        QueryPerformanceCounter(&timestamp_raw);
        auto timestamp = timestamp_raw.QuadPart * 1000000 / perf_freq.QuadPart;
        // const auto timestamp_ptr = static_cast<uint8_t*>(static_cast<void*>(&timestamp));
        // const std::vector<uint8_t> timestamp_bytes(timestamp_ptr, timestamp_ptr + sizeof timestamp);
        // const auto qr = QrCode::encodeBinary(timestamp_bytes, static_cast<QrCode::Ecc>(ecc));
        const auto qr = QrCode::encodeText(fmt::format("{} {}", frame_counter, timestamp).c_str(),
                                           static_cast<QrCode::Ecc>(ecc));
        // const auto qr = QrCode::encodeText(
        //     fmt::format("{} {}", std::numeric_limits<uint32_t>::max(),
        //     std::numeric_limits<LONGLONG>::max())
        //         .c_str(),
        //     static_cast<QrCode::Ecc>(ecc));

        // Draw background.
        canvas.SetColor(255u, 255u, 255u, alpha);
        canvas.DrawRect({x_pos, y_pos},
                        {x_pos + (qr.getSize() + 2) * scale, y_pos + (qr.getSize() + 2) * scale});
        // Draw foreground.
        canvas.SetColor(0u, 0u, 0u, alpha);
        for (auto y = 0; y < qr.getSize(); y++) {
            for (auto x = 0; x < qr.getSize(); x++) {
                if (qr.getModule(x, y)) {
                    canvas.DrawRect({(x + 1) * scale + x_pos, (y + 1) * scale + y_pos},
                                    {(x + 2) * scale + x_pos, (y + 2) * scale + y_pos});
                }
            }
        }
    }
}

void QRPlugin::onUnload() {
}

} // namespace QRPlugin
